package com.avatarmaker.app;

import com.avatarmaker.app.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    @Query(value = "FROM Item i WHERE i.gender = 'M' ORDER BY i.name")
    List<Item> findAllM();

    @Query(value = "FROM Item i WHERE i.gender = 'F' ORDER BY i.name")
    List<Item> findAllF();
}
