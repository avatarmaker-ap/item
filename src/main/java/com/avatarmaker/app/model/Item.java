package com.avatarmaker.app.model;

import javax.persistence.*;

@Entity
@Table(name = "item")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "url")
    private String url;

    @Column(name = "gender")
    private String gender;

    @Column(name = "type")
    private String type;

    public Item(){

    }

    public Item(String name, String url, String gender, String type){
        this.name = name;
        this.url = url;
        this.gender = gender;
        this.type = type;
    }

    public Long getId() {
        return id;
    }
	
	public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public String getGender() {
        return gender;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setType(String type){
        this.type = type;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public void setGender(String gender){
        this.gender = gender;
    }

}
