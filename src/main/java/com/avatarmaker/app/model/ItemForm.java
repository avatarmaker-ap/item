package com.avatarmaker.app.model;

public class ItemForm {
    private String name;
    private String url;
    private String gender;
    private String type;

    public ItemForm(){
    }

    public ItemForm(String name, String url, String gender, String type){
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public String getGender() {
        return gender;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setType(String type){
        this.type = type;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public void setGender(String gender){
        this.gender = gender;
    }

}
