package com.avatarmaker.app.controller;

import com.avatarmaker.app.ItemRepository;
import com.avatarmaker.app.exception.ItemNotFoundException;
import com.avatarmaker.app.model.Item;
import com.avatarmaker.app.model.ItemForm;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = {"http://localhost:3000", "https://avatarmaker.netlify.com", "https://avatarmaker.netlify.app"})
public class ItemController {

    private ItemRepository itemRepository;

    public ItemController(ItemRepository itemRepository){
        this.itemRepository = itemRepository;
    }

    @GetMapping("/items")
    public List<Item> getItems() {
        return itemRepository.findAll();
    }

    @GetMapping("/items/m")
    public List<Item> getItemsM() { return itemRepository.findAllM();
    }

    @GetMapping("/items/f")
    public List<Item> getItemsF() { return itemRepository.findAllF();
    }

    @GetMapping("/items/{id}")
    public Item getItemById(@PathVariable Long id) { return itemRepository.findById(id)
            .orElseThrow(() -> new ItemNotFoundException("Item with id " + id + " not found"));
    }

    @PutMapping("/items/{id}")
    public Item updateItem(@PathVariable Long id, @Valid @RequestBody ItemForm itemForm) {
        return itemRepository.findById(id)
                .map(itm -> {
                    itm.setName(itemForm.getName());
                    itm.setType(itemForm.getType());
                    itm.setUrl(itemForm.getUrl());
                    itm.setGender(itemForm.getGender());
                    return itemRepository.save(itm);
                })
                .orElseThrow(() -> new ItemNotFoundException("Item with id " + id + " not found"));
    }

    @DeleteMapping("/items/{id}")
    public List<Item> deleteItem(@PathVariable Long id){
        if (itemRepository.findById(id).isPresent()){
            itemRepository.deleteById(id);
            return itemRepository.findAll();
        }
        else throw new ItemNotFoundException("Item with id " + id + " not found");
    }

    @PostMapping("/items")
    public Item saveItem(@Valid @RequestBody ItemForm itemForm) {
        Item item = new Item(itemForm.getName(), itemForm.getUrl(), itemForm.getGender(), itemForm.getType());
        return itemRepository.save(item);
    }
}
